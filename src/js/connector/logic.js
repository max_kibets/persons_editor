export const getConnectionId = () => {
    return (new URLSearchParams(window.location.search)).get('id');
};