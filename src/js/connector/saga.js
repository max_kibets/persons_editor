import axios from 'axios';
import configMain from '../config/config';
import configURL from '../config/requestConfigs';
import { eventChannel } from 'redux-saga';
import { takeEvery, put, call, select, take } from 'redux-saga/effects';
import ActionTypes from '../constants/actionTypes';
import * as actions from './actions';
import * as logic from './logic';
import * as selectors from './selectors';

export default function* connectorSaga() {
    // yield takeEvery(ActionTypes.SEND_REQUEST, sendRequest);

    yield takeEvery(ActionTypes.LOGIN, login);
    yield takeEvery(ActionTypes.LOGOUT, logout);
    yield takeEvery(ActionTypes.FETCH_EDITORS_DATA, fetchEditorsData);
    yield takeEvery(ActionTypes.FETCH_EDIT_PAGE_DATA, fetchEditPageData);
    yield takeEvery(ActionTypes.SEND_EDITS, sendEdits);
    yield takeEvery(ActionTypes.FETCH_LOGS_DATA, fetchLogsData);
    yield takeEvery(ActionTypes.ADD_EDITOR_FIELD, sendNewEditorField);
    yield takeEvery(ActionTypes.EDIT_EDITOR_FIELD, sendEditedEditorField);
    yield takeEvery(ActionTypes.DELETE_EDITOR_FIELD, sendDeletedEditorField);
}

export function request(params) {
    return eventChannel(emitter => {
        const { method, url, data } = params.requestParams;

        axios[method](`${configURL.base}${url}`, JSON.stringify(data))
            .then(res => {
                const { isError, ...data } = res.data;

                if (isError) {
                    emitter({
                        type: `${params.target}_failure`,
                        payload: data,
                    });
                } else {
                    emitter({
                        type: `${params.target}_success`,
                        payload: data,
                    });
                }
            })
            .catch(err => {
                emitter({
                    type: `connection_failure`,
                    payload: {
                        code: 'NETWORK_ERROR',
                        data: err.message,
                    },
                });
                throw new Error(err);
            });

        return () => {};
    });
}

export function* sendRequest(action) {
    if (!action || !action.payload || !action.payload.target || !action.payload.requestParams) {
        return false;
    }

    const channel = yield call(request, action.payload);
    const response = yield take(channel);
    
    yield put(response);
    channel.close();
}

export function* login(action) {
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'post',
            url: configURL.auth,
            data: action.payload,
        },
    });

    yield call(sendRequest, actionSendRequest);
}

export function* logout(action) {
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'post',
            url: configURL.logout,
            data: {
                connectionId,
            },
        },
    });

    yield call(sendRequest, actionSendRequest);
}

//fetchEditorsData the same as fetchLogsData.... should reorganize
export function* fetchEditorsData(action) {
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'get',
            url: `${configURL.persons}?id=${connectionId}&version=${version}`,
        },
    });

    yield call(sendRequest, actionSendRequest);
}

//fetchEditorsData the same as fetchLogsData.... should reorganize
export function* fetchLogsData(action) {
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'get',
            url: `${configURL.logs}?id=${connectionId}&version=${version}`,
        },
    });

    yield call(sendRequest, actionSendRequest);
}

export function* fetchEditPageData(action) {
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'get',
            url: `${configURL.person}?id=${connectionId}&version=${version}&personId=${action.payload.editorId}`,
        },
    });

    yield call(sendRequest, actionSendRequest);
}

export function* sendEdits(action) {
    const url = action.payload.merge ? configURL.editAndMerge : configURL.edit;
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'post',
            url,
            data: {
                ...action.payload.editorFields,
                version,
                searchingValue: configMain.searchingValue,
                id: connectionId,
            },
        },
    });

    yield call(sendRequest, actionSendRequest);
}

// the same.. [sendNewEditorField, sendEditedEditorField, sendDeletedEditorField]
export function* sendNewEditorField(action) {
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'post',
            url: configURL.addField,
            data: {
                ...action.payload,
                version,
                id: connectionId,
            },
        },
    });

    yield call(sendRequest, actionSendRequest);
}

export function* sendEditedEditorField(action) {
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'post',
            url: configURL.editField,
            data: {
                ...action.payload,
                version,
                id: connectionId,
            },
        },
    });

    yield call(sendRequest, actionSendRequest);
}

export function* sendDeletedEditorField(action) {
    const version = yield select(selectors.getVersion);
    const connectionId = yield call(logic.getConnectionId);
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'post',
            url: configURL.deleteField,
            data: {
                ...action.payload,
                version,
                id: connectionId,
            },
        },
    });

    yield call(sendRequest, actionSendRequest);
}