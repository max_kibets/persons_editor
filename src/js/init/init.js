'use strict';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { compose, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../roots/rootReducer';
import rootSaga from '../roots/rootSaga';
import configCore from '../config/config';
import RootModule from '../modules/rootModule/';

document.addEventListener('DOMContentLoaded', () => {
    init();
});

function init() {
    document.title = configCore.title;
    setFavIcon(configCore.favIcon);

    const initialState = {
        authPage: {
            active: true,
            write: false,
            userFields: {
                name: 'adolf',
                pass: '1488',
                db: 'hw_uidb',
                table: 'persons',
            },
        },
        tabs: {
            editors: true,
            logs: false,
        },
        tabsData: {
            version: configCore.version,
            editorsData: [],
            logsData: [],
        },
        editPage: {
            active: false,
            editorFields: {},
        },
        settingsModal: {
            active: false,
            fields: {
                addField: '',
                editField: '',
            },
            selects: {
                editSelect: '',
                deleteSelect: '',
            },
        },
    };
    
    const sagaMiddleware = createSagaMiddleware({
        onError: error => {
            alert('Critical error acquired! See console for more details.');
            console.error(error);
            sagaMiddleware.run(rootSaga);
        },
    });
    
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(sagaMiddleware)),
    );

    sagaMiddleware.run(rootSaga);

    render(
        <Provider store={store}>
            <RootModule/>
        </Provider>,
        document.getElementById('root')
    );
}

export function setFavIcon(src) {
    if (!src) {
        return false;
    }

    const favicon = document.createElement('link');

    favicon.rel = 'icon';
    favicon.href = src;
    document.head.appendChild(favicon);
}
