import { all, call } from 'redux-saga/effects';
import modelSaga from '../model/saga';
import connectorSaga from '../connector/saga';

const sagasList = [
    modelSaga,
    connectorSaga,
];

export default function* watchRootSaga() {
    yield all(sagasList.map(saga => call(saga)));
}
