import { combineReducers } from 'redux';
import authPageReducer from '../model/reducers/authPageReducer';
import headerReducer from '../model/reducers/headerReducer';
import tabsReducer from '../model/reducers/tebsReducer';
import editorsReducer from '../model/reducers/editorsReducer';
import logsReducer from '../model/reducers/logsReducer';
import editPageReducer from '../model/reducers/editPageReducer';
import settingsModalReducer from '../model/reducers/settingsModalReducer';

const rootReducer = combineReducers({
    authPage: authPageReducer,
    tabs: tabsReducer,
    tabsData: combineReducers({
        version: headerReducer,
        editorsData: editorsReducer,
        logsData: logsReducer,
    }),
    editPage: editPageReducer,
    settingsModal: settingsModalReducer,
});

export default rootReducer;
