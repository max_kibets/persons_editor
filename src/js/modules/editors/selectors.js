export const getWrite = state => state.authPage.write;
export const getEditorsData = state => state.tabsData.editorsData;