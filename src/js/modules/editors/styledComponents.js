import styled from 'styled-components';

const Editor = styled.div`
    display: flex;
    align-items: center;
    border: 1px solid #000;
    border-bottom: none;
    padding: 10px;
`;

const lastEditor = styled(Editor)`
    border-bottom: 1px solid #000;
`;

const Id = styled.div`
    margin-right: 15px;
    border: 1px solid #000;
    padding: 5px;
    width: 16px;
    border-radius: 50%;
    text-align: center;
`;

const Btn = styled.button`
    border: 1px solid #000;
    padding: 8px 15px;
    color: #000; 
    cursor: pointer;
    text-transform: uppercase;
    line-height: 15px;
    text-decoration: none;
    text-align: center;
`;

const gray = styled(Btn)`
    background-color:#ddd;
`;

const editorBtn = styled(Btn)`
    margin-left: auto;
`;

Editor.Id = Id;
Editor.last = lastEditor;
Btn.gray = gray;
Btn.editor = editorBtn;

export {
    Editor,
    Btn,
}