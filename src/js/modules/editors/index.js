import Editors from './Editors.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    write: selectors.getWrite(state),
    editorsData: selectors.getEditorsData(state),
});

export const mapDispatchToProps = dispatch => ({
    fetchEditorsData: () => dispatch(actions.fetchEditorsData()),
    showEditPage: payload => dispatch(actions.fetchEditPageData(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Editors);