import ActionTypes from '../../constants/actionTypes';

export const fetchEditorsData = () => ({ type: ActionTypes.FETCH_EDITORS_DATA });
export const fetchEditPageData = payload => ({ type: ActionTypes.FETCH_EDIT_PAGE_DATA, payload });
