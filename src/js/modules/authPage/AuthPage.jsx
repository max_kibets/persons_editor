import React from 'react';
import PropTypes from 'prop-types';
import PureComponent from '../../base/PureComponent.jsx';
import ErrorBoundary from '../../decorators/ErrorBoundary.jsx';
import InputRow from './components/inputRow/InputRow.jsx';
import { Form } from './styledComponents';
import strings from '../../strings/en';
import { getConnectionId } from '../../connector/logic';

@ErrorBoundary
export default class AuthPage extends PureComponent {
    componentDidMount() {
        const connectionId = getConnectionId();

        connectionId && this.props.login({ connectionId });
    }

    handlerSubmit = (event) => {
        event.preventDefault();

        const { userFields, login } = this.props;
        
        login(userFields);
    }

    getInputRows = () => {
        const { updateUserFields, userFields } = this.props;

        return Object.keys(userFields).map(item => {
            return (
                <InputRow 
                    key={item}
                    name={item}
                    type={item === 'pass' ? 'password' : 'text'}
                    label={strings.resources.form[item]}
                    value={userFields[item]}
                    updateUserFields={updateUserFields}/>
            );
        });
    }

    render() {
        return (
            <Form.Wrap>
                <Form onSubmit={this.handlerSubmit}>
                    {this.getInputRows()}
                    
                    <Form.RowBtn>
                        <Form.Btn.green>
                            {strings.resources.buttons.enter}
                        </Form.Btn.green>
                    </Form.RowBtn>
                </Form>
            </Form.Wrap>
        )
    };
}

AuthPage.propTypes = {
    userFields: PropTypes.object.isRequired,
    updateUserFields: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired
};