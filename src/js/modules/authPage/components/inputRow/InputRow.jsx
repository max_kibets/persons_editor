import React from 'react';
import PropTypes from 'prop-types';
import { Form } from '../../styledComponents';

const InputRow = ({ label, type, name, value, updateUserFields }) => {
    const handlerChange = (event) => {
        updateUserFields({
            value: event.target.value,
            name: event.target.name,
        });
    };

    return (
        <Form.Row>
            <Form.Label>{label}</Form.Label>
            <Form.Input
                type={type}
                name={name}
                value={value}
                onChange={handlerChange}
                required/>
        </Form.Row>
    );
};

InputRow.propTypes = {
    type: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    updateUserFields: PropTypes.func.isRequired,
};

export default React.memo(InputRow);