import ActionTypes from '../../constants/actionTypes';

export const sendEdits = payload => ({ type: ActionTypes.SEND_EDITS, payload });
export const updateEditPage = payload => ({ type: ActionTypes.UPDATE_EDIT_PAGE_STORE, payload });
export const updateEditorFields = payload => ({ type: ActionTypes.UPDATE_EDITOR_FIELDS_STORE, payload });