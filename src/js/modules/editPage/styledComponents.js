import styled from 'styled-components';

const Wrap = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    display: flex;
    z-index: 999;
    background-color: #ffffff;
`;

const Form = styled.div`
    margin: auto;
    border: 1px solid #000;
    padding: 25px;
`;

const Row = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 15px;
`;

const RowBtn = styled(Row)`
    justify-content: center;
    margin: 0 -5px;
`;

const Label = styled.label`
    display: inline-block;
    min-width: 90px;
`;

const Textarea = styled.textarea`
    padding: 8px 10px;
    border: 1px solid #000;
    width: 100%;
    resize: none;
`;

const Btn = styled.button`
    margin: 0 5px;
    border: 1px solid #000;
    padding: 8px 15px;
    width: 100%;
    color: #000; 
    cursor: pointer;
    text-transform: uppercase;
    line-height: 15px;
    text-decoration: none;
    text-align: center;
    white-space: nowrap;
`;

const green = styled(Btn)`
    background-color: #87d483;
`;

const gray = styled(Btn)`
    background-color:#ddd;
`;

Form.Wrap = Wrap;
Form.Row = Row;
Form.RowBtn = RowBtn;
Form.Label = Label;
Form.Textarea = Textarea;
Btn.green = green;
Btn.gray = gray;
Form.Btn = Btn;

export { Form };