import React from 'react';
import PropTypes from 'prop-types';
import strings from '../../strings/en';
import config from '../../config/config';
import { HeaderBlock, Select, Btn } from './styledComponents';

const Header = ({ changeVersion, logout, currentVersion, showSettingsModal }) => {
    const handlerChange = (event) => {
        changeVersion(event.target.value);
    };

    const options = config.versionOptions.map((version) => {
        return (
            <option key={version} value={version}>{strings.resources.select[version]}</option>
        );
    });

    return (
        <HeaderBlock>
            <Select
                onChange={handlerChange}
                defaultValue={currentVersion}>
                {options}
            </Select>

            <Btn.gray onClick={showSettingsModal}>{strings.resources.buttons.settings}</Btn.gray>

            <Btn.gray onClick={logout}>{strings.resources.buttons.logout}</Btn.gray>
        </HeaderBlock>
    );
};

Header.propTypes = {
    changeVersion: PropTypes.func.isRequired,
    currentVersion: PropTypes.string.isRequired,
    logout: PropTypes.func.isRequired,
    showSettingsModal: PropTypes.func.isRequired,
};

export default React.memo(Header);