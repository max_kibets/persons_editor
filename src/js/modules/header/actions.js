import ActionTypes from '../../constants/actionTypes';

export const logout = () => ({ type: ActionTypes.LOGOUT });
export const changeVersion = payload => ({ type: ActionTypes.CHANGE_VERSION, payload });
export const showSettingsModal = () => ({ type: ActionTypes.TOGGLE_SETTINGS_MODAL });