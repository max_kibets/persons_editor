import styled from 'styled-components';

const TabsBlock = styled.div`
    display: flex;
    box-shadow: inset 0 -1px 0 #000;
`;

const TabItem = styled.div`
    border-right: 1px solid #000;
    border-bottom: 1px solid #000;
    padding: 15px;
    cursor: pointer;
    text-transform: uppercase;
    background-color: #fff;
`;

const activeTab = styled(TabItem)`
    border-bottom-color: transparent;
`;

TabsBlock.Item = TabItem;
TabItem.active = activeTab;

export {
    TabsBlock,
}