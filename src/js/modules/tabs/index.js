import Tabs from './Tabs.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    tabs: selectors.getTabs(state),
});

export const mapDispatchToProps = dispatch => ({
    toggleTabs: (payload) => dispatch(actions.toggleTabs(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tabs);