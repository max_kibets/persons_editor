import ActionTypes from '../../constants/actionTypes';

export const toggleTabs = (payload) => ({ type: ActionTypes.TOGGLE_TABS, payload });