import RootModule from './RootModule.jsx';
import { connect } from 'react-redux';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    showAuthPage: selectors.getAuthPageStatus(state),
    showEditPage: selectors.getEditPageStatus(state),
});

export default connect(
    mapStateToProps
)(RootModule);