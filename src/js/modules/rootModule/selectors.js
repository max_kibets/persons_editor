export const getAuthPageStatus = state => state.authPage.active;
export const getEditPageStatus = state => state.editPage.active;