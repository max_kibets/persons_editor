import mainPage from './mainPage.jsx';
import { connect } from 'react-redux';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    tabs: selectors.getTabs(state),
});

export default connect(
    mapStateToProps,
)(mainPage);