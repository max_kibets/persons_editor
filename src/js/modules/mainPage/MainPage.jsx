import React from 'react';
import PropTypes from 'prop-types';
import PureComponent from '../../base/PureComponent.jsx';
import ErrorBoundary from '../../decorators/ErrorBoundary.jsx';
import Header from '../header/';
import Tabs from '../tabs/';
import Editors from '../editors/';
import Logs from '../logs/';
import { Container, Content } from './styledComponents';

@ErrorBoundary
export default class MainPage extends PureComponent {
    getContent = () => {
        const { tabs } = this.props;

        return (
            <React.Fragment>
                {tabs.editors ? <Editors/> : null}
                {tabs.logs ? <Logs/> : null}
            </React.Fragment>
        )
    };

    render() {
        return (
            <React.Fragment>
                <Header/>
                <Container>
                    <Tabs/>
                    <Content>
                        {this.getContent()}
                    </Content>
                </Container>
            </React.Fragment>
        )
    }
};

MainPage.propTypes = {
    tabs: PropTypes.object.isRequired,
};