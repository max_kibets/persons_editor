import Logs from './Logs.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    logsData: selectors.getLogsData(state),
});

export const mapDispatchToProps = dispatch => ({
    fetchLogsData: () => dispatch(actions.fetchLogsData()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Logs);