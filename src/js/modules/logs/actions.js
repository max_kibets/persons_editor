import ActionTypes from '../../constants/actionTypes';

export const fetchLogsData = () => ({ type: ActionTypes.FETCH_LOGS_DATA });