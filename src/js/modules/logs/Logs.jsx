import React from 'react';
import PropTypes from 'prop-types';
import PureComponent from '../../base/PureComponent.jsx';
import strings from '../../strings/en';
import config from '../../config/config.js';
import { TableBlock } from './styledComponents';
import HtmlDiff from 'htmldiff-js';
import ReactHtmlParser from 'react-html-parser'; 

export default class Logs extends PureComponent {
    componentDidMount() {
        this.props.fetchLogsData();
    }

    getRows = () => {
        return this.props.logsData.map((log, index) => {
            const { user, dateTime, searchingValue, changed, oldValue, newValue } = log;
            const diffStr = HtmlDiff.execute(oldValue, newValue);

            return (
                // key={index} ---- какашка!
                <tr key={index}>
                    <TableBlock.Td>{ user }</TableBlock.Td>
                    <TableBlock.Td>{ dateTime }</TableBlock.Td>
                    <TableBlock.Td>{ searchingValue }</TableBlock.Td>
                    <TableBlock.Td>{ changed }</TableBlock.Td>
                    <TableBlock.Td>{ ReactHtmlParser(diffStr) }</TableBlock.Td>
                </tr>
            )
        });
    }

    render() {
        const { user, date, changed, diff } = strings.resources.table;

        return (
            <TableBlock>
                <thead>
                    <tr>
                        <TableBlock.Th>{ user }</TableBlock.Th>
                        <TableBlock.Th>{ date }</TableBlock.Th>
                        <TableBlock.Th>{ config.searchingValueColumn }</TableBlock.Th>
                        <TableBlock.Th>{ changed }</TableBlock.Th>
                        <TableBlock.Th>{ diff }</TableBlock.Th>
                    </tr>
                </thead>
                <tbody>
                    { this.getRows() }
                </tbody>
            </TableBlock>
        );
    }
}

Logs.propTypes = {
    logsData: PropTypes.array.isRequired,
    fetchLogsData: PropTypes.func.isRequired,
};