export const getSettingsModalStatus = state => state.settingsModal.active;
export const getEditorFields = state => state.tabsData.editorsData[0];
export const getSettingsModalFields = state => state.settingsModal.fields;
export const getSettingsModalSelects = state => state.settingsModal.selects;