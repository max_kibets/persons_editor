import styled from 'styled-components';

const Wrap = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    display: flex;
    z-index: 999;
    background-color: #ffffff;
`;

const Form = styled.form`
    margin: auto;
    border: 1px solid #000;
    padding: 25px;
`;

const Section = styled.div`
    margin-bottom: 15px;
    padding: 15px;
    border: 1px solid #000;
`;

const SectionLast = styled(Section)`
    margin-bottom: 0;
`;

const Heading = styled.div`
    margin-bottom: 15px;
    text-transform: uppercase;
    text-align: center;
    font-size: 16px;
`;

const HeadingSmall = styled(Heading)`
    font-size: 13px;
`

const Row = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 15px;
`;

const RowBtn = styled(Row)`
    justify-content: center;
    margin: 0 -5px 0;
`;

const Label = styled.label`
    display: inline-block;
    min-width: 90px;
    white-space: nowrap;
`;

const Input = styled.input`
    padding: 8px 10px;
    border: 1px solid #000;
    width: 100%;
`;

const Textarea = styled.textarea`
    padding: 8px 10px;
    border: 1px solid #000;
    width: 100%;
`;

const Btn = styled.button`
    border: 1px solid #000;
    margin: 0 5px;
    padding: 8px 15px;
    width: 100%;
    color: #000; 
    cursor: pointer;
    text-transform: uppercase;
    line-height: 15px;
    text-decoration: none;
    text-align: center;
    white-space: nowrap;
`;

const green = styled(Btn)`
    background-color: #87d483;
`;

const Select = styled.select`
    width: 100%;
    padding: 6px 10px;
    border-color: black;
    outline: none;
`;

const Close = styled.div`
    position: absolute;
    top: 10px;
    right: 10px;
    width: 20px;
    height: 20px;
    cursor: pointer;

    &:before {
        content: '';
        position: absolute;
        top: 50%;
        width: 100%;
        height: 1px;
        background-color: #000;
        transform: rotate(45deg);
        transform-origin: 50%;
    }

    &:after {
        content: '';
        position: absolute;
        top: 50%;
        width: 100%;
        height: 1px;
        background-color: #000;
        transform: rotate(-45deg);
        transform-origin: 50%;
    }
`;

const modalStyles = {
    content: {
        top: '50%',
        right: 'auto',
        bottom: 'auto',
        left: '50%',
        border: '1px solid #000',
        padding: '25px',
        borderRadius: '0',
        transform: 'translate(-50%, -50.1%)',
    },
    overlay: {
        backgroundColor: 'rgb(0,0,0, 0.5)',
    },
};

Form.Wrap = Wrap;
Form.Section = Section;
Section.last = SectionLast;
Form.Heading = Heading;
Heading.small = HeadingSmall;
Form.Row = Row;
Form.RowBtn = RowBtn;
Form.Label = Label;
Form.Input = Input;
Form.Textarea = Textarea;
Form.Select = Select;
Btn.green = green;
Form.Close = Close;

export { Form, Btn, modalStyles };
