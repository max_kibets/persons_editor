import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import strings from '../../strings/en';
import { modalStyles, Form, Btn } from './styledComponents';

Modal.setAppElement('#root');

const SettingsModal = ({
    isActive,
    hideSettingsModal,
    editorFields,
    settingsFields,
    settingsSelects,
    updateSettingsFields,
    updateSettingsSelects,
    addEditorField,
    editEditorField,
    deleteEditorField,
}) => {
    const handlerClose = () => {
        hideSettingsModal({ active: !isActive });
    };

    const handlerAddField = () => {
        addEditorField({
            key: settingsFields.addField,
        });
    };

    const handlerAddFieldAndMerge = () => {
        addEditorField({ 
            key: settingsFields.addField,
            isMerge: true,
        });
    };

    const handlerEditField = () => {
        editEditorField({
            key: settingsFields.editField,
            oldKey: settingsSelects.editSelect,
        });
    };

    const handlerEditFieldAndMerge = () => {
        editEditorField({
            key: settingsFields.editField,
            oldKey: settingsSelects.editSelect,
            isMerge: true,
        });
    };

    const handlerDeleteField = () => {
        deleteEditorField({
            key: settingsSelects.deleteSelect,
        });
    };

    const handlerDeleteFieldAndMerge = () => {
        deleteEditorField({ 
            key: settingsSelects.deleteSelect,
            isMerge: true,
        });
    };

    const handlerChangeInput = (event) => {
        updateSettingsFields({
            value: event.target.value,
            name: event.target.name,
        });
    };

    const handlerChangeSelect = (event) => {
        updateSettingsSelects({
            value: event.target.value,
            name: event.target.name,
        });
    };

    const getSelects = () => {
        const selects = { ...settingsSelects };

        for (const selectName in selects) {
            selects[selectName] = <Form.Select
                name={selectName}
                onChange={handlerChangeSelect}
                value={settingsSelects[selectName]}>
                    <option disabled value=''>{strings.resources.select.default}</option>
                    {getOptions()}
                </Form.Select>
        }
        
        return selects;
    };

    const getOptions = () => {
        return editorFields && Object.keys(editorFields).map(fieldName => {
            if (fieldName !== 'id') {
                return (
                    <option key={fieldName} value={fieldName}>{fieldName}</option>
                );
            }
        });
    };

    const getInputs = () => {
        const inputs = { ...settingsFields };

        for (const name in inputs) {
            inputs[name] = <Form.Input 
                name={name}
                value={settingsFields[name]}
                onChange={handlerChangeInput}></Form.Input>;
        }

        return inputs;
    };

    return (
        <Modal
            isOpen={isActive}
            style={modalStyles}>
            <Form.Close onClick={handlerClose}/>

            <Form.Heading>{strings.resources.headings.settings}</Form.Heading>

            <Form.Section>
                <Form.Heading.small>{strings.resources.headings.addField}</Form.Heading.small>
                <Form.Row>
                    <Form.Label>{strings.resources.form.name}</Form.Label>
                    {getInputs().addField}
                </Form.Row>
                <Form.RowBtn>
                    <Btn.green onClick={handlerAddField}>{strings.resources.buttons.add}</Btn.green>
                    <Btn.green onClick={handlerAddFieldAndMerge}>{strings.resources.buttons.addToAll}</Btn.green>
                </Form.RowBtn>
            </Form.Section>
            
            <Form.Section>
                <Form.Heading.small>{strings.resources.headings.editField}</Form.Heading.small>
                <Form.Row>
                    <Form.Label>{strings.resources.form.fields}</Form.Label>
                    {getSelects().editSelect}
                </Form.Row>
                <Form.Row>
                    <Form.Label>{strings.resources.form.newName}</Form.Label>
                    {getInputs().editField}
                </Form.Row>
                <Form.RowBtn>
                    <Btn.green onClick={handlerEditField}>{strings.resources.buttons.add}</Btn.green>
                    <Btn.green onClick={handlerEditFieldAndMerge}>{strings.resources.buttons.addToAll}</Btn.green>
                </Form.RowBtn>
            </Form.Section>
            
            <Form.Section.last>
                <Form.Heading.small>{strings.resources.headings.deleteField}</Form.Heading.small>
                <Form.Row>
                    <Form.Label>{strings.resources.form.name}</Form.Label>
                    {getSelects().deleteSelect}
                </Form.Row>
                <Form.RowBtn>
                    <Btn.green onClick={handlerDeleteField}>{strings.resources.buttons.delete}</Btn.green>
                    <Btn.green onClick={handlerDeleteFieldAndMerge}>{strings.resources.buttons.deleteToAll}</Btn.green>
                </Form.RowBtn>
            </Form.Section.last>
        </Modal>
    );
};

SettingsModal.propTypes = {
    isActive: PropTypes.bool.isRequired,
    hideSettingsModal: PropTypes.func.isRequired,
    editorFields: PropTypes.object,
    settingsFields: PropTypes.object.isRequired,
    settingsSelects: PropTypes.object.isRequired,
    updateSettingsFields: PropTypes.func.isRequired,
    updateSettingsSelects: PropTypes.func.isRequired,
    addEditorField: PropTypes.func.isRequired,
    editEditorField: PropTypes.func.isRequired,
    deleteEditorField: PropTypes.func.isRequired,
};

export default React.memo(SettingsModal);