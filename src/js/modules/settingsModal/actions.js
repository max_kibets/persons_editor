import ActionTypes from '../../constants/actionTypes';

export const hideSettingsModal = payload => ({ type: ActionTypes.TOGGLE_SETTINGS_MODAL, payload });
export const updateSettingsFields = payload => ({ type: ActionTypes.UPDATA_SETTINGS_MODAL_FIELDS_STORE, payload });
export const updateSettingsSelects = payload => ({ type: ActionTypes.UPDATA_SETTINGS_MODAL_SELECTS_STORE, payload });
export const addEditorField = payload => ({ type: ActionTypes.ADD_EDITOR_FIELD, payload });
export const editEditorField = payload => ({ type: ActionTypes.EDIT_EDITOR_FIELD, payload });
export const deleteEditorField = payload => ({ type: ActionTypes.DELETE_EDITOR_FIELD, payload });