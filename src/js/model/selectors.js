export const getVersion = state => state.tabsData.version;
export const getEditPageStatus = state => state.editPage.active;
export const getTabs = state => state.tabs;
export const getSettingsModalStatus = state => state.settingsModal.active;