import ActionTypes from '../../constants/actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.UPDATE_TABS_STORE:
            return {
                ...state,
                ...action.payload,
            };

        default: {
            return state;
        }
    }
};