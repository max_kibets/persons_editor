import ActionTypes from '../../constants/actionTypes';

export default (state = null, action) => {
    switch (action.type) {
        case ActionTypes.UPDATE_VERSION_STORE:
            return action.payload;

        default: {
            return state;
        }
    }
};