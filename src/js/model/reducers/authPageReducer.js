import ActionTypes from '../../constants/actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.UPDATE_AUTH_PAGE_STORE:
            return {
                ...state,
                ...action.payload,
            };
            
        case ActionTypes.UPDATE_USER_FIELDS_STORE:
            return {
                ...state,
                userFields: {
                    ...state.userFields,
                    [action.payload.name]: action.payload.value,
                },
            };

        default: {
            return state;
        }
    }
};