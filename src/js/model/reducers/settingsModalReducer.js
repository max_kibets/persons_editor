import ActionTypes from '../../constants/actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.TOGGLE_SETTINGS_MODAL_STORE:
            return {
                ...state,
                ...action.payload,
            };

        case ActionTypes.UPDATA_SETTINGS_MODAL_FIELDS_STORE:
            return {
                ...state,
                fields: {
                    ...state.fields,
                    [action.payload.name]: action.payload.value,
                },
            };

        case ActionTypes.UPDATA_SETTINGS_MODAL_SELECTS_STORE:
            return {
                ...state,
                selects: {
                    ...state.selects,
                    [action.payload.name]: action.payload.value,
                },
            };

        case ActionTypes.RESET_SETTINGS_MODAL_SELECTS_STORE:
            return {
                ...state,
                selects: {
                    ...state.selects,
                    editSelect: '',
                    deleteSelect: '',
                },
            };

        default: {
            return state;
        }
    }
};