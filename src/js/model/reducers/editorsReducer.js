import ActionTypes from '../../constants/actionTypes';

export default (state = [], action) => {
    switch (action.type) {
        case ActionTypes.UPDATE_EDITORS_DATA_STORE:
            return action.payload;

        default: {
            return state;
        }
    }
};