import { takeEvery, put, call, select } from 'redux-saga/effects';
import ActionTypes from '../constants/actionTypes';
import * as actions from './actions';
import * as selectors from './selectors';
import * as logic from './logic';

export default function* modelSaga() {
    yield takeEvery(ActionTypes.LOGIN_SUCCESS, login);
    yield takeEvery(ActionTypes.LOGOUT_SUCCESS, logout);
    yield takeEvery(ActionTypes.FETCH_EDITORS_DATA_SUCCESS, updateEditors);
    yield takeEvery(ActionTypes.FETCH_LOGS_DATA_SUCCESS, updateLogs);
    yield takeEvery(ActionTypes.FETCH_EDIT_PAGE_DATA_SUCCESS, toggleEditPage);
    yield takeEvery(ActionTypes.SEND_EDITS_SUCCESS, toggleEditPage);
    yield takeEvery(ActionTypes.TOGGLE_TABS, toggleTabs);
    yield takeEvery(ActionTypes.CHANGE_VERSION, changeVerison);
    yield takeEvery(ActionTypes.TOGGLE_SETTINGS_MODAL, toggleSettingsModal);
    yield takeEvery([
        ActionTypes.ADD_EDITOR_FIELD_SUCCESS,
        ActionTypes.EDIT_EDITOR_FIELD_SUCCESS,
        ActionTypes.DELETE_EDITOR_FIELD_SUCCESS,
    ], settingsModalActionDone);
    
    yield takeEvery([
        ActionTypes.LOGIN_FAILURE,
        ActionTypes.LOGOUT_FAILURE,
        ActionTypes.FETCH_EDITORS_DATA_FAILURE,
        ActionTypes.FETCH_LOGS_DATA_FAILURE,
        ActionTypes.FETCH_EDIT_PAGE_DATA_FAILURE,
        ActionTypes.SEND_EDITS_FAILURE,
        ActionTypes.ADD_EDITOR_FIELD_FAILURE,
        ActionTypes.EDIT_EDITOR_FIELD_FAILURE,
        ActionTypes.DELETE_EDITOR_FIELD_FAILURE,
        ActionTypes.CONNECTION_FAILURE,
    ], error);
}

function* login(action) {
    const loginData = yield call(logic.login, action.payload);
    yield put(actions.updateAuthPage(loginData));
}

function* logout(action) {
    const logoutData = yield call(logic.logout, action.payload);
    yield put(actions.updateAuthPage(logoutData));
}

function* updateEditors(action) {
    yield put(actions.updateEditorsData(
        Object.values(action.payload)
    )); 
}

function* updateLogs(action) {
    yield put(actions.updateLogsData(
        Object.values(action.payload)
    ));
}

function* toggleEditPage(action) {
    const status = yield select(selectors.getEditPageStatus);
    const editPageData = yield call(logic.getEditPageData, status, action.payload);
    yield put(actions.updateEditPage(editPageData));
}

function* toggleTabs(action) {
    const tabs = yield select(selectors.getTabs);
    const currentTabs = yield call(logic.toggleObjectFlags, tabs, action.payload);
    yield put(actions.updateTabs(currentTabs));
}

function* changeVerison(action) {
    const { editors, logs } = yield select(selectors.getTabs);
    yield put(actions.updateVersion(action.payload));
    logs && (yield put(actions.fetchLogsData()));
    editors && (yield put(actions.fetchEditorsData()));
}

function* toggleSettingsModal() {
    const status = yield select(selectors.getSettingsModalStatus);
    yield put(actions.toggleSettingsModal({ active: !status }));
}

function* settingsModalActionDone(action) {
    yield put(actions.updateEditorsData(
        Object.values(action.payload)
    ));
    yield put(actions.resetSettingsModalSelects());
    yield call(logic.showSuccessMessage, action.type);
}

// failure
function* error(action) {
    yield call(logic.showErrorMessage, action.payload);
}