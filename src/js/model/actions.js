import ActionTypes from '../constants/actionTypes';

export const updateAuthPage = payload => ({ type: ActionTypes.UPDATE_AUTH_PAGE_STORE, payload });
export const updateEditorsData = payload => ({ type: ActionTypes.UPDATE_EDITORS_DATA_STORE, payload });
export const updateLogsData = payload => ({ type: ActionTypes.UPDATE_LOGS_DATA_STORE, payload });
export const updateEditPage = payload => ({ type: ActionTypes.UPDATE_EDIT_PAGE_STORE, payload });
export const updateTabs = payload => ({ type: ActionTypes.UPDATE_TABS_STORE, payload });
export const updateVersion = payload => ({ type: ActionTypes.UPDATE_VERSION_STORE, payload });
export const fetchEditorsData = () => ({ type: ActionTypes.FETCH_EDITORS_DATA });
export const fetchLogsData = () => ({ type: ActionTypes.FETCH_LOGS_DATA });
export const toggleSettingsModal = payload => ({ type: ActionTypes.TOGGLE_SETTINGS_MODAL_STORE, payload });
export const resetSettingsModalSelects = () => ({ type: ActionTypes.RESET_SETTINGS_MODAL_SELECTS_STORE });