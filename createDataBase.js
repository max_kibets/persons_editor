const mysql = require('mysql2');

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "hw_uidb",
    password: ""
}).promise();

let dataArray1 = [
    {
        id: '1',
        name: 'Leonid',
        surname: 'Makarovich',
        address: 'Rivne',
        gender: 'male',
        age: '1',
    },
    {
        id: '2',
        name: 'Leonid',
        surname: 'Danylovich',
        address: 'Dnipro',
        gender: 'male',
        age: '1',
    },
    {
        id: '3',
        name: 'Victor',
        surname: 'Andrijovych',
        address: 'Vulyk',
        gender: 'male',
        age: '1',
    },
    {
        id: '4',
        name: 'Victor',
        surname: 'Fedorovych',
        address: 'Bambass',
        gender: 'male',
        age: '1',
    },
    {
        id: '5',
        name: 'Petro',
        surname: 'Oleksiyovych',
        address: 'Vinnytsja',
        gender: 'male',
        age: '1',
    },
];

let dataArray2 = [
    {
        id: '1',
        name: 'Leonid',
        surname: 'Makarovich',
        address: 'Rivne',
        gender: 'male',
        age: '2',
    },
    {
        id: '2',
        name: 'Leonid',
        surname: 'Danylovich',
        address: 'Dnipro',
        gender: 'male',
        age: '2',
    },
    {
        id: '3',
        name: 'Victor',
        surname: 'Andrijovych',
        address: 'Vulyk',
        gender: 'male',
        age: '2',
    },
    {
        id: '4',
        name: 'Victor',
        surname: 'Fedorovych',
        address: 'Bambass',
        gender: 'male',
        age: '2',
    },
    {
        id: '5',
        name: 'Petro',
        surname: 'Oleksiyovych',
        address: 'Vinnytsja',
        gender: 'male',
        age: '2',
    },
];

let dataArray3 = [
    {
        id: '1',
        name: 'Leonid',
        surname: 'Makarovich',
        address: 'Rivne',
        gender: 'male',
        age: '3',
    },
    {
        id: '2',
        name: 'Leonid',
        surname: 'Danylovich',
        address: 'Dnipro',
        gender: 'male',
        age: '3',
    },
    {
        id: '3',
        name: 'Victor',
        surname: 'Andrijovych',
        address: 'Vulyk',
        gender: 'male',
        age: '3',
    },
    {
        id: '4',
        name: 'Victor',
        surname: 'Fedorovych',
        address: 'Bambass',
        gender: 'male',
        age: '3',
    },
    {
        id: '5',
        name: 'Petro',
        surname: 'Oleksiyovych',
        address: 'Vinnytsja',
        gender: 'male',
        age: '3',
    },
];

let dataArray4 = [
    {
        id: '1',
        name: 'Leonid',
        surname: 'Makarovich',
        address: 'Rivne',
        gender: 'male',
        age: '4',
    },
    {
        id: '2',
        name: 'Leonid',
        surname: 'Danylovich',
        address: 'Dnipro',
        gender: 'male',
        age: '4',
    },
    {
        id: '3',
        name: 'Victor',
        surname: 'Andrijovych',
        address: 'Vulyk',
        gender: 'male',
        age: '4',
    },
    {
        id: '4',
        name: 'Victor',
        surname: 'Fedorovych',
        address: 'Bambass',
        gender: 'male',
        age: '4',
    },
    {
        id: '5',
        name: 'Petro',
        surname: 'Oleksiyovych',
        address: 'Vinnytsja',
        gender: 'male',
        age: '4',
    },
];

let emails = ['vergunda.marketservice@gmail.com', 'dev.comdex@gmail.com', 'kibetss3@gmail.com', 'opelat.a@gmail.com'];



function createTablePerson() {
    const SQLQueryCreateTablePersons = "CREATE TABLE persons (id INT(8), version1 VARCHAR(255), version2 VARCHAR(255), version3 VARCHAR(255), version4 VARCHAR(255))";
    connection.query(SQLQueryCreateTablePersons)
    .then(() => {
        console.log('Table persons created');
    })
    .catch(err => {
        console.error(err);
    });
}

function createTableLogs() {
    const SQLQueryCreateTableLogs = "CREATE TABLE logs (user VARCHAR(16), dateTime VARCHAR(16), SearchingValue VARCHAR(16), changed VARCHAR(16), oldValue VARCHAR(64), newValue VARCHAR(64))";
    connection.query(SQLQueryCreateTableLogs)
        .then(() => {
            console.log('Table logs created');
        })
        .catch(err => {
            console.error(err);
        });
}

function createTableEmails() {
    const SQLQueryCreateTableEmails = "CREATE TABLE emails (id VARCHAR(16), email VARCHAR(64))";
    connection.query(SQLQueryCreateTableEmails)
    .then(() => {
        console.log('Table emails created');
    })
    .catch(err => {
        console.error(err);
    });
}

function insertPersons() {
    for (let index = 0; index < dataArray1.length; index++) {
        const SQLQueryInsertPersons = `INSERT INTO persons (id, version1, version2, version3, version4) VALUES (${Number(dataArray1[index].id)}, '${JSON.stringify(dataArray1[index])}', '${JSON.stringify(dataArray2[index])}', '${JSON.stringify(dataArray3[index])}', '${JSON.stringify(dataArray4[index])}')`
        connection.query(SQLQueryInsertPersons)
            .then(() => {
                console.log('Persons data inserted into persons table');
            })
            .catch(err => {
                console.error(err);
            });
    }
}

function insertEmails() {
    for (let index = 0; index < emails.length; index++) {
        const SQLQueryInsertEmails = `INSERT INTO emails (id, email) VALUES (${index}, '${emails[index]}')`;
        connection.query(SQLQueryInsertEmails)
        .then(() => {
            console.log('Email data inserted into Emails table');
        })
            .catch(err => {
                console.error(err);
            });
    }
}


// createTablePerson();
// createTableLogs();
// createTableEmails();
insertPersons();
// insertEmails();

